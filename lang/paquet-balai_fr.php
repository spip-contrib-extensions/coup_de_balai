<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'balai_description' => 'Permet "nettoyer" un site en supprimant tous les articles et toutes les rubriques à l\'exception de ceux et celles qui auront été explicitement protégés.',
	'balai_nom' => 'Coup de balai',
	'balai_slogan' => 'Ménage de printemps',
);
?>
